# 理财系统接口测试

## 本项目涉及接口

- 注册：获取图片验证码，获取短信验证码，注册
- 登录：登录，登录状态查询
- 开户：实名认证，开户，第三方开户接口
- 充值：获取充值验证码，充值，第三方充值接口
- 投资：投资，第三方投资接口

## 工具实现

### Postman

实现注册登录和认证接口的全局变量保存及提取，及结果的断言。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0126/123057_0ab364cc_9560547.png "屏幕截图.png")

### Jmeter

实现全接口测试，涉及用户定义的变量、JDBC请求项目数据库、json断言及响应断言、JSON/正则表达式提取、接口关联等。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0126/123113_fd9516bd_9560547.png "屏幕截图.png")

## 代码实现

获取图片验证码，获取短信验证码，登录，认证

### 接口自动化测试环境搭建

python第三方库：requests,unittest, HTMLTestRunner, parameterized, os, logging, pymysql, json

### 测试框架说明

apiTest # 项目名称<br>
├── api # 封装接口URL，定义接口方法及其入参，得到响应<br>
&emsp;&emsp;├──Login.py<br>
&emsp;&emsp;├──Register.py<br>
&emsp;&emsp;├──GetCode.py<br>
&emsp;&emsp;├──Approve.py<br>
├── data # 存放json格式的测试数据<br>
&emsp;&emsp;├──login.json<br>
&emsp;&emsp;├──register.json<br>
&emsp;&emsp;├──imgVerify.json<br>
&emsp;&emsp;├──approve.json<br>
&emsp;&emsp;├──smsVerify.json<br>
├── report # 保存由HTMLTextRunner得到的测试报告<br>
&emsp;&emsp;├──report.html<br>
├── script # 基于unittest定义测试用例脚本<br>
&emsp;&emsp;├──test_ImgCode.py<br>
&emsp;&emsp;├──test_SmsCode.py<br>
&emsp;&emsp;├──test_Register.py<br>
&emsp;&emsp;├──test_login.py<br>
&emsp;&emsp;├──test_approve.py<br>
├── log # 存放日志文件<br>
&emsp;&emsp;├──p2p.log<br>
├──tools # 存放第三方工具<br>
&emsp;&emsp;├──HTMLTextRunner.py<br>
├── config.py # 定义项目的配置信息，如日志初始化，项目路径与基本URL<br>
├── utils.py # 定义工具类，实现参数化测试数据的读取<br>
└── run_suite.py # 将测试用例集成为测试套件，执行后生成测试报告<br>

### 测试报告

![输入图片说明](https://images.gitee.com/uploads/images/2022/0126/123126_1d0056b6_9560547.png "屏幕截图.png")

## 接口缺陷

![输入图片说明](https://images.gitee.com/uploads/images/2022/0126/123138_e6c067d5_9560547.png "屏幕截图.png")

## 项目难点

- 未掌握postman连接数据库清除注册成功的手机号，导致注册成功用例是一次性的，且由于各脚本手机号不同导致不同的cookie，引发很多问题。目前仅会使用Jmeter的JDBC Configuration Pool及JDBC request元件进行数据库连接解决这个问题。
- 开户请求脚本开发困难在于：请求开户接口响应需要进行正则表达式提取，需要细心和耐心提取正确的响应内容。
- 涉及项目数据库请求需要提前熟悉表结构，先清理依赖表再清理主表。写删除SQL时尤其注意，不能多删。