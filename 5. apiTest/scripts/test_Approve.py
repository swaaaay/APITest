import logging
from parameterized import parameterized
import requests
import unittest
from utils import get_case_data, assert_utils
from api.Login import LoginAPI
from api.Approve import ApproveAPI


class TestApprove(unittest.TestCase):

    def setUp(self) -> None:
        self.api = ApproveAPI()
        self.session = requests.Session()

    def tearDown(self) -> None:
        self.session.close()

    @parameterized.expand(get_case_data(
        "approve.json", "approve", "realname,card_id,status_code,status,desc"))
    def test_param(self, realname, card_id, status_code, status, desc):
        LoginAPI().login(self.session, "15912345678", "oo0000")
        response = self.api.approve(self.session, realname, card_id)
        logging.info("approve response = {}".format(response.json()))
        self.assertEqual(status_code, response.status_code)
        self.assertEqual(status, response.json().get("status"))
