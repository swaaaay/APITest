import logging
import random

from parameterized import parameterized
import requests
import unittest
from utils import get_case_data
from api.GetCode import GetCodeAPI


class TestSmsCode(unittest.TestCase):
    def setUp(self) -> None:
        self.api = GetCodeAPI()
        self.session = requests.Session()

    def tearDown(self) -> None:
        self.session.close()

    @parameterized.expand(get_case_data(
        "smsVerify.json", "get_smsverify", "phone,imcode,status_code,status,desc"))
    def test_param(self, phone, imcode, status_code, status, desc):
        # 1、获取图片验证码
        self.api.getImgCode(self.session, str(random.random()))
        # 2、获取短信验证码
        response = self.api.getSmsCode(self.session, phone, imcode)
        logging.info("get sms code response = {}".format(response.json()))
        self.assertEqual(status_code, response.status_code)
        self.assertEqual(status, response.json().get("status"))
        self.assertEqual(desc, response.json().get("description"))
