import logging
import unittest, requests, random
from api.GetCode import GetCodeAPI
from utils import get_case_data
from parameterized import parameterized


class TestImgCode(unittest.TestCase):
    def setUp(self) -> None:
        self.api = GetCodeAPI()
        self.session = requests.Session()

    def tearDown(self) -> None:
        self.session.close()

    @parameterized.expand(get_case_data("imgVerify.json", "get_imgverify", "type,status_code"))
    def test_param(self, type, status_code):
        r = ''
        if type == 'float':
            r = str(random.random())
        elif type == 'int':
            r = str(random.randint(10000000, 90000000))
        elif type == 'char':
            r = ''.join(random.sample("abcdedfhijklmn", 8))
        # 发送请求
        response = self.api.getImgCode(self.session, r)
        logging.info("r = {} response = {}".format(r, response))
        # 对响应结果进行断言
        self.assertEqual(status_code, response.status_code)
