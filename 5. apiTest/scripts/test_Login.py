import logging
from parameterized import parameterized
import requests
import unittest
from utils import get_case_data
from api.Login import LoginAPI


class TestLogin(unittest.TestCase):
    def setUp(self) -> None:
        self.api = LoginAPI()
        self.session = requests.Session()

    @parameterized.expand(get_case_data(
        "login.json", "login", "keywords,password,status_code,status,desc"))
    def test_param(self, keywords, password, status_code, status, desc):
        response = self.api.login(self.session, keywords, password)
        logging.info("login response = {}".format(response.json()))
        self.assertEqual(status_code, response.status_code)
        self.assertEqual(status, response.json().get("status"))
        self.assertIn(desc, response.json().get("description"))

