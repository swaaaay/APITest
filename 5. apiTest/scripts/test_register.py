import logging
import unittest
import random
import requests
from parameterized import parameterized
from tools.DButils import DBUtil
from utils import get_case_data
from api.GetCode import GetCodeAPI
from api.Register import RegisterAPI


class Register(unittest.TestCase):
    imgCode = '8888'
    smsCode = '666666'

    @classmethod
    def setUpClass(cls) -> None:
        sql1 = "delete from mb_member_register_log where phone in ('13033447711','13033447712','13033447713'," \
               "'13033447714','13033447715'); "
        DBUtil.idu_one(sql1)
        logging.info("delete sql = {}".format(sql1))
        sql2 = "delete i.* from mb_member_login_log i INNER JOIN mb_member m on i.member_id = m.id WHERE m.phone in (" \
               "'13033447711','13033447712','13033447713','13033447714','13033447715'); "
        DBUtil.idu_one(sql2)
        logging.info("delete sq2 = {}".format(sql2))
        sql3 = "delete i.* from mb_member_info i INNER JOIN mb_member m on i.member_id = m.id WHERE m.phone in (" \
               "'13033447711','13033447712','13033447713','13033447714','13033447715'); "
        DBUtil.idu_one(sql3)
        logging.info("delete sq2 = {}".format(sql3))
        sql4 = "delete from mb_member WHERE phone in ('13033447711','13033447712','13033447713','13033447714'," \
               "'13033447715'); "
        DBUtil.idu_one(sql4)
        logging.info("delete sq2 = {}".format(sql4))

    def setUp(self) -> None:
        self.code = GetCodeAPI()
        self.reg_api = RegisterAPI()
        self.session = requests.Session()

    def tearDown(self) -> None:
        self.session.close()

    @parameterized.expand(get_case_data("register.json", "test_register",
                                        "phone,pwd,imgVerifyCode,phoneCode,dyServer,invite_phone,status_code,"
                                        "status,description"))
    def test10_register(self, phone, pwd, imgVerifyCode, phoneCode, dyServer, invite_phone, status_code, status,
                        description):
        # 1、获取图片验证码成功
        r = random.random()
        # 调用接口类中的接口
        self.code.getImgCode(self.session, str(r))
        # 2、获取短信验证码成功
        self.code.getSmsCode(self.session, phone, self.imgCode)
        # 3、使用参数化的测试数据进行注册，并返回对应的结果
        # 发送注册请求
        response = self.reg_api.register(self.session, phone, pwd, imgVerifyCode, phoneCode, dyServer, invite_phone)
        logging.info("register response = {}".format(response.json()))
        # 对收到的响应进行断言
        self.assertEqual(status_code, response.status_code)
        self.assertEqual(status, response.json().get("status"))
        self.assertIn(description, response.json().get("description"))
