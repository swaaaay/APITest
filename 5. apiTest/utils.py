import json
import config
import logging
import requests, unittest
from bs4 import BeautifulSoup


def get_case_data(filename, method_name, param_names):
    """参数化文件内容获取
    filename： 参数数据文件的文件名
    method_name: 参数数据文件中定义的测试数据列表的名称，如：test_get_img_verify_code
    param_names: 参数数据文件一组测试数据中所有的参数组成的字符串，如："type,status_code"""

    # 获取测试数据文件的文件路径
    file = config.BASE_DIR + "/data/" + filename
    test_case_data = []
    with open(file, encoding="utf-8") as f:
        # 将json字符串转换为字典格式
        file_data = json.load(f)
        # 获取所有的测试数据的列表
        test_data_list = file_data.get(method_name)
        for test_data in test_data_list:
            # 先将test_data对应的一组测试数据，全部读取出来，并生成一个列表
            test_params = []
            for param in param_names.split(","):
                # 依次获取同一组测试数中每个参数的值，添加到test_params中，形成一个列表
                test_params.append(test_data.get(param))
            # 每完成一组测试数据的读取，就添加到test_case_data后，直到所有的测试数据读取完毕
            test_case_data.append(test_params)
    print("test_case_data = {}".format(test_case_data))
    return test_case_data


def request_third_api(form_data):
    # 解析form表单中的内容，并提取第三方请求的参数
    soup = BeautifulSoup(form_data, "html.parser")
    third_url = soup.form['action']
    logging.info("third request url = {}".format(third_url))
    data = {}
    for input in soup.find_all('input'):
        data.setdefault(input['name'], input['value'])
    logging.info("third request data = {}".format(data))
    # 发送第三方请求
    response = requests.post(third_url, data=data)
    return response


def assert_utils(self, response, status_code, status, desc):
    unittest.TestCase.assertEqual(self, status_code, response.status_code)
    unittest.TestCase.assertEqual(self, status, response.json().get('status'))
    unittest.TestCase.assertIn(self, desc, response.json().get('description'))


html = """
<html>
<head><title>黑马程序员</title></head>
<body>
<p id="test01">软件测试</p>
<p id="test02">2020年</p>
<a href="/api.html">接口测试</a>
<a href="/web.html">Web自动化测试</a>
<a href="/app.html">APP自动化测试</a>
</body>
</html>
"""
soup = BeautifulSoup(html, 'html.parser')
# 获取title的对象
print(soup.title)
# 获取title的标签名称
print(soup.title.name)
# 获取title的值
print(soup.title.string)

# 获取p的对象
print(soup.p)
# 获取所有的P的对象
print(soup.find_all('p'))
# 获取第一个P标签对应的ID属性的值
print(soup.p['id'])

# # 依次打印出所有A标签的href属性的值和A标签的值
# for s in soup.find_all('a'):
#     print("href={} text={}".format(s['href'], s.string))
