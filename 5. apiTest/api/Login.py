import config


class LoginAPI:
    def __init__(self):
        self.login_url = config.BASE_URL + '/member/public/login/'

    def login(self, session, phone, pswd):
        data = {'keywords': phone, 'password': pswd}
        response = session.post(self.login_url, data=data)
        return response
