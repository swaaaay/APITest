import config


class RegisterAPI():
    def __init__(self):
        self.register_url = config.BASE_URL + '/member/public/reg'

    def register(self, session, phone, pwd, imgVerifyCode='8888', phoneCode='666666'
                 , dyServer='on', invite_phone=''):
        data = {"phone": phone,
                "password": pwd,
                "verifycode": imgVerifyCode,
                "phone_code": phoneCode,
                "dy_server": dyServer,
                'invite_phone': invite_phone}
        response = session.post(self.register_url, data=data)
        return response

