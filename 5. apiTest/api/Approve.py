import config


class ApproveAPI:
    def __init__(self):
        self.approve_url = config.BASE_URL + '/member/realname/approverealname'

    def approve(self, session, name, id):
        data = {'realname': name, 'card_id': id}
        response = session.post(self.approve_url, data=data)
        return response
