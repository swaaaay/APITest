import config


class GetCodeAPI:
    def __init__(self):
        self.getImgCode_url = config.BASE_URL + '/common/public/verifycode1/'
        self.getSmsCode_url = config.BASE_URL + '/member/public/sendSms/'

    def getImgCode(self, session, r):
        response = session.get(self.getImgCode_url + r)
        return response

    def getSmsCode(self, session, phone, imcode):
        data = {'phone': phone, 'imgVerifyCode': imcode, type: 'reg'}
        response = session.post(self.getSmsCode_url, data=data)
        return response
