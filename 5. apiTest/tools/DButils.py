import pymysql, config


class DBUtil:
    __conn = None

    # 获取数据库连接对象
    @classmethod
    def get_conn(cls):
        if cls.__conn is None:
            cls.__conn = pymysql.connect(
                host=config.DB_URL,
                user="root", password=config.DB_PSWD, database=config.DB_NAME,
                port=3306, charset='utf8')
        return cls.__conn

    # 关闭数据库连接对象
    @classmethod
    def close_conn(cls):
        if cls.__conn:
            cls.__conn.close()
            cls.__conn = None

    # 获取游标对象
    @classmethod
    def get_cursor(cls):
        return cls.get_conn().cursor()

    # 关闭游标对象
    @classmethod
    def close_cursor(cls, cursor):
        if cursor: cursor.close()

    @classmethod
    def select_one(cls, sql):
        cursor = DBUtil.get_cursor()
        cursor.execute(sql)
        # conn.commit() # DML操作需要
        result = cursor.fetchone()
        DBUtil.close_cursor(cursor)
        DBUtil.close_conn()
        return result

    @classmethod
    def idu_one(cls, sql):
        conn = DBUtil.get_conn()
        cursor = conn.cursor()
        cursor.execute(sql)
        conn.commit()  # DML操作需要
        result = cursor.fetchone()
        DBUtil.close_cursor(cursor)
        DBUtil.close_conn()
        return result
#
# if __name__ == '__main__':
#     # 检验是否连接成功
#     result = DBUtil.select_one("select count(*) from litemall_user;")
#     print(result)
