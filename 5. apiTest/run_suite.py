import unittest
from scripts.test_ImgCode import TestImgCode
from scripts.test_SmsCode import TestSmsCode
from scripts.test_Login import TestLogin
from scripts.test_Approve import TestApprove
from tools.HTMLTestRunner import HTMLTestRunner
import config

suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestImgCode))
suite.addTest(unittest.makeSuite(TestSmsCode))
suite.addTest(unittest.makeSuite(TestLogin))
suite.addTest(unittest.makeSuite(TestApprove))
report_file = config.BASE_DIR + "/report/report.html"
with open(report_file, 'wb') as f:
    runner = HTMLTestRunner(f, title="理财系统接口测试报告")
    runner.run(suite)
